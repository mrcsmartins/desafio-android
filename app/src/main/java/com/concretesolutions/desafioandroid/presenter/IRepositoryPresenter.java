package com.concretesolutions.desafioandroid.presenter;

import android.support.v4.app.FragmentActivity;

/**
 * Created by Soua on 19/11/2015.
 */
public interface IRepositoryPresenter {
    void fail(String error);

    FragmentActivity getLocalActivity();
    void downloadRepositories(int page);

    void downloadPulls(String owner, String repository);

    void showNextView();

    boolean onBackPressed();

    void updateRepository(String result);

    void updatePull(String result);

    void openBrowser(String url);

    void back();

    void setTitle(String title);
}
