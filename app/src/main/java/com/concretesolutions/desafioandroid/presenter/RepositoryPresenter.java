package com.concretesolutions.desafioandroid.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.concretesolutions.desafioandroid.AppController;
import com.concretesolutions.desafioandroid.R;
import com.concretesolutions.desafioandroid.model.json.scheme.JSONPullScheme;
import com.concretesolutions.desafioandroid.model.json.scheme.JSONRepositoryScheme;
import com.concretesolutions.desafioandroid.model.task.RequestTask;
import com.concretesolutions.desafioandroid.view.RepositoryView;
import com.concretesolutions.desafioandroid.view.adapter.PullAdapter;
import com.concretesolutions.desafioandroid.view.adapter.RepositoryAdapter;
import com.concretesolutions.desafioandroid.presenter.events.RepositoryActionListener;
import com.concretesolutions.desafioandroid.view.interfaces.IRepositoryView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;


public class RepositoryPresenter extends Fragment implements IRepositoryPresenter  {
    private static final String IS_DETAIL = "IS_DETAIL";
    private static final String TITLE = "TITLE";
    private static final String PULL_REQUEST_TITLE = "PULL_REQUEST_TITLE";
    private FragmentActivity activity;
    private RepositoryAdapter repositoryAdapter;
    private int page = 1;
    private IRepositoryView repositoryView;
    private PullAdapter pullAdapter;
    private AsyncTask<String, String, String> repositoryRequestTask;
    private AsyncTask<String, String, String> pullRequestTask;
    private String title;
    private String pullRequestTitle;

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            this.activity = (FragmentActivity) activity;
        }
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i("desafio", "desafio.savedInstanceState - " + savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle bundle) {
        final ViewGroup view = (ViewGroup) inflater.inflate(R.layout.app_repository, container, false);
        this.createView(view);
        setRetainInstance(true);
        return view;
    }

    private void createView(final ViewGroup view) {
        repositoryView = new RepositoryView(view);
        if(repositoryAdapter==null){
            this.repositoryView.showProgressBar(View.VISIBLE);
            repositoryAdapter = new RepositoryAdapter(this);
            this.downloadRepositories(this.page);
        }

        repositoryView.setRepositoryAdapter(repositoryAdapter);
        repositoryView.setEndlessScrollListener(new RepositoryActionListener.EndlessScrollActionListener(this));
        repositoryView.setOnClickListener(new RepositoryActionListener.ButtonsOnClickActionListener(this));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState!=null){
           if(savedInstanceState.getBoolean(IS_DETAIL)&&pullAdapter!=null&&savedInstanceState.getString(TITLE)!=null&&savedInstanceState.getString(PULL_REQUEST_TITLE)!=null){
               this.repositoryView.setTitle(savedInstanceState.getString(TITLE));
               this.repositoryView.setPullTitle(savedInstanceState.getString(PULL_REQUEST_TITLE));
               repositoryView.setPullAdapter(pullAdapter);
               this.repositoryView.nextView();
           }
        }
    }

    @Override
    public void fail(String error) {
        Log.i("desafio", "desafio.onFailure - " + error);
        this.repositoryView.showProgressBar(View.GONE);
    }

    @Override
    public FragmentActivity getLocalActivity(){
        return  this.getActivity();
    }

    @Override
    public void downloadRepositories(int page) {
        this.page = page;
        if(this.page>1){
            this.repositoryView.showProgressBar(View.VISIBLE);
        }
        repositoryRequestTask = new RequestTask(new RepositoryActionListener.JSONRepositoyCallBack(this)).execute("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=" + page);
    }

    @Override
    public void downloadPulls(String owner, String repository) {
        this.repositoryView.showProgressBar(View.VISIBLE);
        this.repositoryView.setPullTitle("");
        this.pullAdapter = new PullAdapter(this);
        repositoryView.setPullAdapter(pullAdapter);
        pullRequestTask = new RequestTask(new RepositoryActionListener.JSONPullCallBack(this)).execute("https://api.github.com/repos/"+owner+"/"+repository+"/pulls");
    }

    @Override
    public void showNextView() {
        this.repositoryView.showProgressBar(View.GONE);
        this.repositoryView.nextView();
    }

    @Override
    public void updateRepository(String result) {
        Log.i("challenge", "Json:" + result);
        final Gson gson = new Gson();
        JSONRepositoryScheme sucessJSON = gson.fromJson(result, JSONRepositoryScheme.class);
        Log.i("challenge", "Json:success -" + gson.toJson(sucessJSON));
        if(sucessJSON!=null&&sucessJSON.getRepositories()!=null){
            repositoryAdapter.update(sucessJSON.getRepositories());
            Log.i("desafio","desafio.onSuccess - "+String.valueOf(sucessJSON.getTotalCount()));
        }
        this.repositoryRequestTask = null;
        this.repositoryView.showProgressBar(View.GONE);
    }

    @Override
    public void updatePull(String result) {
        Log.i("challenge", "Json:" + result);
        final Gson gson = new Gson();
        final Type listType = new TypeToken<List<JSONPullScheme>>(){}.getType();
        final List<JSONPullScheme> sucessJSON = gson.fromJson(result,listType);
        Log.i("challenge", "Json:success -" + gson.toJson(sucessJSON));
        if(sucessJSON != null&&pullAdapter != null) {
            this.pullRequestTitle = "Pull Request Count: "+sucessJSON.size();
            this.repositoryView.setPullTitle(this.pullRequestTitle);
            pullAdapter.update(sucessJSON);
        }
        this.repositoryView.showProgressBar(View.GONE);
        this.pullRequestTask = null;
    }

    @Override
    public void openBrowser(String url) {
        final Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void back() {
        this.repositoryView.setTitle(getString(R.string.title_repository));
        if(this.getLocalActivity() instanceof AppController){
            ((AppController) this.getLocalActivity()).onBackPressed();
        }
    }

    @Override
    public boolean onBackPressed() {
        this.repositoryView.setTitle(getString(R.string.title_repository));
        if(this.repositoryView.isDetail()){
            this.repositoryView.nextView();
            return false;
        }else{
            return true;
        }
    }

    @Override
    public Context getContext() {
        return super.getContext();
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
        this.repositoryView.setTitle(title);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(IS_DETAIL, this.repositoryView.isDetail());
        outState.putString(TITLE, this.title);
        outState.putString(PULL_REQUEST_TITLE, this.pullRequestTitle);
        super.onSaveInstanceState(outState);
    }
}