package com.concretesolutions.desafioandroid.presenter.events;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.concretesolutions.desafioandroid.model.json.scheme.JSONRepositoryScheme;
import com.concretesolutions.desafioandroid.model.task.RequestTask;
import com.concretesolutions.desafioandroid.presenter.IRepositoryPresenter;
import com.concretesolutions.desafioandroid.view.components.EndlessScrollListener;

public class RepositoryActionListener {

    public static class ButtonsOnClickActionListener implements OnClickListener {
        private final  IRepositoryPresenter presenter;
        public ButtonsOnClickActionListener(final IRepositoryPresenter presenter) {
            this.presenter = presenter;
        }

        @Override
        public void onClick(final View view) {
            this.presenter.back();
        }
    }

    public static class ButtonsItemOnClickActionListener implements OnClickListener {
        private final  IRepositoryPresenter presenter;
        private final  JSONRepositoryScheme JSONRepositoryScheme;
        public ButtonsItemOnClickActionListener(final IRepositoryPresenter presenter,JSONRepositoryScheme JSONRepositoryScheme, int position) {
            this.presenter            = presenter;
            this.JSONRepositoryScheme = JSONRepositoryScheme;
        }

        @Override
        public void onClick(final View view) {
            this.presenter.setTitle(this.JSONRepositoryScheme.getName());
            this.presenter.showNextView();
            this.presenter.downloadPulls(this.JSONRepositoryScheme.getOwner().getLogin(),this.JSONRepositoryScheme.getName());
        }
    }

    public static class ButtonsPullItemOnClickActionListener implements OnClickListener {
        private final  IRepositoryPresenter presenter;
        private final  String url;
        public ButtonsPullItemOnClickActionListener(final IRepositoryPresenter presenter,String url) {
            this.presenter            = presenter;
            this.url   = url;
        }

        @Override
        public void onClick(final View view) {
            this.presenter.openBrowser(this.url);
        }
    }

    public static class EndlessScrollActionListener extends EndlessScrollListener {
        private final transient IRepositoryPresenter presenter;

        public EndlessScrollActionListener(final IRepositoryPresenter presenter) {
            this.presenter = presenter;
        }


        @Override
        public boolean onLoadMore(int page, int totalItemsCount) {
            Log.i("challenge", "onLoadMore:page -" + page+" - totalItemsCount:"+totalItemsCount);
            // Triggered only when new data needs to be appended to the list
            // Add whatever code is needed to append new items to your AdapterView
//            customLoadMoreDataFromApi(page);
            this.presenter.downloadRepositories(page);
            // or customLoadMoreDataFromApi(totalItemsCount);
            return true; // ONLY if more data is actually being loaded; false otherwise.
        }

        // Append more data into the adapter
        public void customLoadMoreDataFromApi(int offset) {
            // This method probably sends out a network request and appends new data items to your adapter.
            // Use the offset value and add it as a parameter to your API request to retrieve paginated data.
            // Deserialize API response and then construct new objects to append to the adapter
        }
    }


    public static class JSONRepositoyCallBack implements RequestTask.IRequestTask {
        private final transient IRepositoryPresenter presenter;

        public JSONRepositoyCallBack(final IRepositoryPresenter presenter) {
            this.presenter = presenter;
        }


        @Override
        public void fail(String error) {
            this.presenter.fail(error);
        }

        @Override
        public void success(String result) {
            this.presenter.updateRepository(result);
        }
    }

    public static class JSONPullCallBack implements RequestTask.IRequestTask {
        private final transient IRepositoryPresenter presenter;

        public JSONPullCallBack(final IRepositoryPresenter presenter) {
            this.presenter = presenter;
        }

        @Override
        public void fail(String error) {
            this.presenter.fail(error);
        }

        @Override
        public void success(String result) {
            this.presenter.updatePull(result);
        }
    }
}
