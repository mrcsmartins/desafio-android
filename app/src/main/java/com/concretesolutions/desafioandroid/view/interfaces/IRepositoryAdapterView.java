package com.concretesolutions.desafioandroid.view.interfaces;

import android.graphics.drawable.Drawable;
import android.view.View;

import com.concretesolutions.desafioandroid.model.task.DownloadImageFromServerTask;
import com.concretesolutions.desafioandroid.presenter.events.RepositoryActionListener;

/**
 * Created by Soua on 19/11/2015.
 */
public interface IRepositoryAdapterView extends DownloadImageFromServerTask.IDownloadImageFromServerTask {
    void setName(String name);

    void setDescription(String description);

    void setForkCount(String forkCount);

    void setStarCount(String starCount);

    void setUserName(String userName);

    void setAvatar(Drawable avatar);

    void setButtonOnclickListener(View.OnClickListener listener);
}
