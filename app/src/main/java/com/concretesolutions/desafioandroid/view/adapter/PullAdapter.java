package com.concretesolutions.desafioandroid.view.adapter;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.concretesolutions.desafioandroid.R;
import com.concretesolutions.desafioandroid.model.json.scheme.JSONPullScheme;
import com.concretesolutions.desafioandroid.model.json.scheme.JSONRepositoryScheme;
import com.concretesolutions.desafioandroid.model.task.DownloadImageFromServerTask;
import com.concretesolutions.desafioandroid.model.task.LruCacheBitmap;
import com.concretesolutions.desafioandroid.presenter.IRepositoryPresenter;
import com.concretesolutions.desafioandroid.presenter.events.RepositoryActionListener;
import com.concretesolutions.desafioandroid.view.RepositoryAdapterView;
import com.concretesolutions.desafioandroid.view.interfaces.IRepositoryAdapterView;

import java.util.ArrayList;
import java.util.List;

public class PullAdapter extends BaseAdapter {

    private transient LayoutInflater inflater;
    private transient IRepositoryPresenter presenter;
    private transient List<JSONPullScheme> pullList;

    public PullAdapter(IRepositoryPresenter presenter) {
        this.presenter = presenter;
        this.inflater = LayoutInflater.from(presenter.getLocalActivity());
        this.pullList = new ArrayList<JSONPullScheme>();
    }

    @Override
    public int getCount() {
        return this.pullList.size();
    }

    @Override
    public JSONPullScheme getItem(int position) {
        return this.pullList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        IRepositoryAdapterView repositoryItemView = null;
        if (convertView == null) {
            convertView =  inflater.inflate(R.layout.app_pull_item, null);
            repositoryItemView = new RepositoryAdapterView((ViewGroup) convertView);
            convertView.setTag(repositoryItemView);
        } else {
            repositoryItemView = (IRepositoryAdapterView) convertView.getTag();
        }
        repositoryItemView.setName(this.getItem(position).getTitle());
        repositoryItemView.setDescription(this.getItem(position).getDescription() + "\n\n" + this.getItem(position).getDate());
        repositoryItemView.setUserName(this.getItem(position).getUser().getLogin());
        if (this.getItem(position).getUser() != null && !this.getItem(position).getUser().getAvatarUrl().equals("")) {
            try {
                Bitmap bitmap = LruCacheBitmap.getBitmapFromMemoryCache(this.getItem(position).getUser().getAvatarUrl());
                if(bitmap==null){
                    new DownloadImageFromServerTask(repositoryItemView).execute(this.getItem(position).getUser().getAvatarUrl());
                }else{
                    repositoryItemView.setImage(bitmap);
                }
            } catch (Exception e) {
                repositoryItemView.setStandardImage();
            }
        }else{
            repositoryItemView.setStandardImage();
        }
        repositoryItemView.setButtonOnclickListener(new RepositoryActionListener.ButtonsPullItemOnClickActionListener(presenter, this.getItem(position).getUrl()));
        return convertView;
    }

    public void update(List<JSONPullScheme> repositoryList){
        this.pullList.addAll(repositoryList);
        this.notifyDataSetChanged();
    }

}
