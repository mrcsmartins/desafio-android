package com.concretesolutions.desafioandroid.view.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.concretesolutions.desafioandroid.R;
import com.concretesolutions.desafioandroid.model.json.scheme.JSONRepositoryScheme;
import com.concretesolutions.desafioandroid.model.task.DownloadImageFromServerTask;
import com.concretesolutions.desafioandroid.model.task.LruCacheBitmap;
import com.concretesolutions.desafioandroid.presenter.IRepositoryPresenter;
import com.concretesolutions.desafioandroid.presenter.events.RepositoryActionListener;
import com.concretesolutions.desafioandroid.view.RepositoryAdapterView;
import com.concretesolutions.desafioandroid.view.interfaces.IRepositoryAdapterView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class RepositoryAdapter extends BaseAdapter {

    private transient LayoutInflater inflater;
    private transient IRepositoryPresenter presenter;
    private transient List<JSONRepositoryScheme> repositoryList;

    public RepositoryAdapter(IRepositoryPresenter presenter) {
        this.presenter = presenter;
        this.inflater = LayoutInflater.from(presenter.getLocalActivity());
        this.repositoryList = new ArrayList<JSONRepositoryScheme>();
    }

    @Override
    public int getCount() {
        return this.repositoryList.size();
    }

    @Override
    public JSONRepositoryScheme getItem(int position) {
        return this.repositoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        IRepositoryAdapterView repositoryItemView = null;
        if (convertView == null) {
            convertView =  inflater.inflate(R.layout.app_repository_item, null);
            repositoryItemView = new RepositoryAdapterView((ViewGroup) convertView);
            convertView.setTag(repositoryItemView);
        } else {
            repositoryItemView = (IRepositoryAdapterView) convertView.getTag();
        }

        repositoryItemView.setName(this.getItem(position).getName());
        repositoryItemView.setDescription(this.getItem(position).getDescription());
        repositoryItemView.setForkCount(String.valueOf(this.getItem(position).getForksCount()));
        repositoryItemView.setStarCount(String.valueOf(this.getItem(position).getStargazersCount()));
        repositoryItemView.setUserName(this.getItem(position).getOwner().getLogin());
        if (this.getItem(position).getOwner() != null && !this.getItem(position).getOwner().getAvatarUrl().equals("")) {
            try {
                Bitmap bitmap = LruCacheBitmap.getBitmapFromMemoryCache(this.getItem(position).getOwner().getAvatarUrl());
                if(bitmap==null){
                    new DownloadImageFromServerTask(repositoryItemView).execute(this.getItem(position).getOwner().getAvatarUrl());
                }else{
                    repositoryItemView.setImage(bitmap);
                }
            } catch (Exception e) {
                repositoryItemView.setStandardImage();
            }
        }else{
            repositoryItemView.setStandardImage();
        }
        repositoryItemView.setButtonOnclickListener(new RepositoryActionListener.ButtonsItemOnClickActionListener(presenter, getItem(position),position));
        return convertView;
    }

    public void update(List<JSONRepositoryScheme> repositoryList){
        this.repositoryList.addAll(repositoryList);
        this.notifyDataSetChanged();
    }

}
