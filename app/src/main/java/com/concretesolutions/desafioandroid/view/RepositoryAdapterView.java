package com.concretesolutions.desafioandroid.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.concretesolutions.desafioandroid.R;
import com.concretesolutions.desafioandroid.view.components.DeclareComponent;
import com.concretesolutions.desafioandroid.view.interfaces.IRepositoryAdapterView;

import java.lang.reflect.Field;

/**
 * Created by Soua on 19/11/2015.
 */
public class RepositoryAdapterView implements IRepositoryAdapterView {
    @DeclareComponent(componentId = R.id.repository_name)
    private TextView repositoryName;
    @DeclareComponent(componentId = R.id.repository_desc)
    private TextView repositoryDescription;
    @DeclareComponent(componentId = R.id.repository_fork)
    private TextView forkCount;
    @DeclareComponent(componentId = R.id.repository_star)
    private TextView starCount;

    @DeclareComponent(componentId = R.id.avatar)
    private ImageView avatar;
    @DeclareComponent(componentId = R.id.user_name)
    private TextView userName;
    private View view;
    public RepositoryAdapterView(final ViewGroup view) {
            this.view = view;
          declareComponentFromView(view, this);
    }



    @Override
    public void setName(String  name){
        this.repositoryName.setText(name);
    }

    @Override
    public void setDescription(String  description){
        this.repositoryDescription.setText(description);
    }

    @Override
    public void setForkCount(String forkCount){
        this.forkCount.setText(forkCount);
    }

    @Override
    public void setStarCount(String starCount){
        this.starCount.setText(starCount);
    }

    @Override
    public void setUserName(String userName){
        this.userName.setText(userName);
    }

    @Override
    public void setAvatar(Drawable avatar){
        this.avatar.setBackground(avatar);
    }

    @Override
    public void setButtonOnclickListener(View.OnClickListener listener) {
        this.view.setOnClickListener(listener);
    }

    public static void declareComponentFromView(final ViewGroup view, Object object) {
        Class clazz = object.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(DeclareComponent.class)) {
                String fieldName = "";
                try {
                    DeclareComponent anotacao = field.getAnnotation(DeclareComponent.class);
                    if (anotacao.componentId() > 0) {
                        fieldName = object.getClass().getDeclaredField(field.getName()).toString();
                        Field fieldCurrent = object.getClass().getDeclaredField(field.getName());
                        fieldCurrent.setAccessible(true);
                        fieldCurrent.set(object, view.findViewById(anotacao.componentId()));
                    }
                } catch (NoSuchFieldException e) {
                    Log.i("AnotaçãoError","AnotaçãoError:" + e.getMessage());
                } catch (IllegalArgumentException e) {
                    Log.i("AnotaçãoError","AnotaçãoError:" + e.getMessage());
                } catch (IllegalAccessException e) {
                    Log.i("AnotaçãoError","AnotaçãoError:" + e.getMessage());
                }
            }
        }
    }

    @Override
    public void showDialog() {

    }

    @Override
    public void dismissDialog() {

    }

    @Override
    public void setImage(Bitmap bitmap) {
        this.avatar.setImageBitmap(bitmap);
    }

    @Override
    public void setStandardImage() {
        this.avatar.setBackgroundResource(R.drawable.git_icon);
    }
}

