package com.concretesolutions.desafioandroid.view.interfaces;

import android.view.View;
import android.widget.BaseAdapter;

import com.concretesolutions.desafioandroid.presenter.events.RepositoryActionListener;

/**
 * Created by Soua on 19/11/2015.
 */
public interface IRepositoryView {
    void setPullTitle(String title);

    void setTitle(String title);

    void nextView();

     boolean isDetail();

    void setRepositoryAdapter(BaseAdapter adapter);

    void setPullAdapter(BaseAdapter adapter);

    void setEndlessScrollListener(RepositoryActionListener.EndlessScrollActionListener endlessScrollActionListener);

    void setOnClickListener(View.OnClickListener listener);

    void showProgressBar(int visibility);
}
