package com.concretesolutions.desafioandroid;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import com.concretesolutions.desafioandroid.model.task.LruCacheBitmap;
import com.concretesolutions.desafioandroid.presenter.IRepositoryPresenter;
import com.concretesolutions.desafioandroid.presenter.RepositoryPresenter;

public class AppController extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_controller);
        LruCacheBitmap.init(getApplicationContext());
        if (savedInstanceState == null) {
            final RepositoryPresenter fragment =  new  RepositoryPresenter();
            this.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_NONE).add(R.id.content, fragment, "RepositoryPresenter").commitAllowingStateLoss();
        }
    }

    @Override
    public void onBackPressed() {
        final RepositoryPresenter fragment =  new  RepositoryPresenter();
        if(this.getSupportFragmentManager().getFragments().size()>0){
             if(this.getSupportFragmentManager().getFragments().get(0) instanceof IRepositoryPresenter){
                 IRepositoryPresenter repositoryPresenter  = (IRepositoryPresenter) this.getSupportFragmentManager().getFragments().get(0);
                 if (repositoryPresenter.onBackPressed()) {
                     super.onBackPressed();
                 }
             }else{
                 super.onBackPressed();
             }
        }else{
            super.onBackPressed();
        }
    }
}
