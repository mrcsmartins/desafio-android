package com.concretesolutions.desafioandroid.model.task;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.support.v4.util.LruCache;

import com.concretesolutions.desafioandroid.R;

import java.io.IOException;


public final class LruCacheBitmap {

	private static Context context;
	private static long allAvailableMem;
	private static InnerLruCache lruCache = null;
	private static final Object SYNC_SING_OBJ = new Object();
	public final static int MEGABITS = 1024 * 1024;
	public final static int BITS = 1024;

	private static final double size = 0.5;

	/**
	 * Block Static to initialize the instance of lruCache 
	 */
	static{
		allAvailableMem = Runtime.getRuntime().maxMemory();
		lruCache = new InnerLruCache( (int) (allAvailableMem * size) );
	}

	/**
	 * Method to set the context on lruCache
	 * @param context the application context
	 */
	public static void init(final Context context){
		context.registerComponentCallbacks(lruCache);
	}

	/**
	 * Method to clear remove the lruCache instance
	 */
	public static void removeLruCacheBitmap() {
		synchronized (SYNC_SING_OBJ) {
    		if (lruCache==null){
    			return;
    		}

    		if(context != null){
        		context.unregisterComponentCallbacks(lruCache);
    		}
    		LruCacheBitmap.lruCache = null;
		}
	}

	/**
	 * 
	 * Inner Class. Extends LruCache.
	 * 
	 */
	public static class InnerLruCache extends LruCache<String, Bitmap> implements ComponentCallbacks2 {

		public InnerLruCache(final int cacheSize) {
			super(cacheSize);
		}

		@Override
		protected int sizeOf(final String key, final Bitmap value) {
			// The cache size will be measured in bytes rather than number of
			// items.
			return value.getByteCount();
		}

		@Override
		protected Bitmap create(final String key) {
			return super.create(key);
		}

		/**
		 * Called by the system when the device configuration changes while your component is running. 
		 */
		@Override
		public void onConfigurationChanged(final Configuration newConfig) {
			// Does nothing.
		}

		@Override
		public void onLowMemory() {
			clearCache();
		}

		@Override
		public void onTrimMemory(int level) {
			if (level >= TRIM_MEMORY_MODERATE) {
				clearCache();
			}
			else if(level >= TRIM_MEMORY_BACKGROUND && lruCache!=null) {
				lruCache.trimToSize(lruCache.size() / 2);
			}
		}

		private void clearCache() {
			lruCache.evictAll();
			Runtime.getRuntime().runFinalization();
		}
	}

	/**
	 * Method add bitmap to lruCache
	 * @param key the key to add on lruCache map
	 * @param bitmap the file to add on lruCache map
	 */
	public static void addBitmapToMemoryCache(final String key, final Bitmap bitmap) {
		if (key != null && bitmap != null) {
			if(!bitmap.isRecycled() && getBitmapFromMemoryCache(key) == null){
				lruCache.put(key, bitmap);
			}
		}
	}

	/**
	 * Method get bitmap to lruCache
	 * @param key the key to get the file from lruCache map
	 * @return Bitmap the file from lruCache map
	 */
	public static synchronized Bitmap getBitmapFromMemoryCache(final String key) {
		final Bitmap bitmap = (Bitmap) lruCache.get(key);
		if(bitmap != null && bitmap.isRecycled()){
			return null;
		}
		else{
			return (Bitmap) lruCache.get(key);
		}
	}

	/**
	 * Method remove bitmap to lruCache
	 * @param key the key to remove the file from lruCache map
	 */
	public static void remove(final String key) {
		if (lruCache!=null){
			lruCache.remove(key);
		}
	}

	/**
	 * Method to calculate the Height by width from image using proportion
	 * @param image to get the actual Height
	 * @param width to calculate the Height using proportion
	 * @return int the Height final
	 */
	public static int calcHeightImage(final Bitmap image, final int width) {
		int calc = 100 * width / image.getWidth();
		return image.getHeight() * calc / 100;
	}

	/**
	 * Method to calculate the Height by width from other font using proportion
	 * @param imgHeight initial from file
	 * @param imgWidth initial from file
	 * @param width the final value to calculate the Height final using proportion
	 * @return int the Height final
	 */
	public static int calcHeight(final int imgHeight, final int imgWidth, final int width) {
		int calc = 0;
		if(imgWidth>0){
			calc = 100 * width / imgWidth;
		}
		return imgHeight * calc / 100;
	}

	/**
	 * Method to verify if has space to create a file on memory by path of file
	 * @param path of the file
	 * @param width initial of the file
	 * @return boolean is can be created
	 */
	public static synchronized boolean verifyCanCreateFile(final String path, final int width) {
		boolean canCreat = false;
		final String keyCache = "verifyCanCreateFile";

		if (path != null) {
			canCreat = true;

			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			remove(keyCache);
			addBitmapToMemoryCache(keyCache, BitmapFactory.decodeFile(path, options));

			int height = 0;

			if (width > 0) {
				height = calcHeight(options.outHeight, options.outWidth, width);

				options.inSampleSize = calculateInSampleSize(options, width, height);
				remove(keyCache);
				addBitmapToMemoryCache(keyCache, BitmapFactory.decodeFile(path,options));
			}

			if (verifyCanCreateBitmap(options.outHeight, options.outWidth)) {
				canCreat = false;
			}
		}
		return canCreat;
	}

	/**
	 * Method to verify if has space to create a file on memory by height and width of file
	 * @param height initial of the file
	 * @param width initial of the file
	 * @return boolean is can be created
	 */
	public static boolean verifyCanCreateBitmap(final int height, final int width) {
		final int finalAvailableMem = (int) (allAvailableMem - Runtime.getRuntime().totalMemory());
		return ((height * width * 4) > finalAvailableMem);
	}

	/**
	 * Method to rotate the bitmap
	 * @param path of the file
	 * @param bitmap the image to rotate
	 * @param oriantationRotate the rotate direction	
	 * @return bitmap rotated
	 */
	private Bitmap rotateBitmap(final String path, final Bitmap bitmap, final int oriantationRotate){
		if(verifyCanCreateFile(path, 0)){
			return postRotateBitmap(bitmap, oriantationRotate, false);
		} else {
			return bitmap;
		}
	}

	/**
	 * Method to know if the file need rotate
	 * @param path of the file
	 * @param bitmap the image to test
	 * @return Integer number of degree to be rotated
	 * @throws IOException if the file of the path not exist
	 */
	private Integer needRotate(final String path, final Bitmap bitmap) throws IOException {
		final ExifInterface exifInterface = new ExifInterface(path);

		if (exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION).equals(""+ExifInterface.ORIENTATION_ROTATE_90)){
			return 90;
		} else if (exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION).equals(""+ExifInterface.ORIENTATION_ROTATE_180)){
			return 180;
		} else if (exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION).equals(""+ExifInterface.ORIENTATION_ROTATE_270)){
			return 270;
		} else {
			return null;
		}
	}

	public Bitmap postRotateBitmap(final Bitmap bitmap, final int rotate, final boolean isMatrixMirror) throws IllegalArgumentException, OutOfMemoryError{

		if (bitmap == null){
			return null;
		}

		// create a matrix for the manipulation
		final Matrix matrix = new Matrix();
		// rotate the Bitmap
		matrix.postRotate(rotate);

		if (isMatrixMirror){
			matrix.preScale(-1.0f, 1.0f);

			float[] mirrorY = { -1, 0, 0, 0, 1, 0, 0, 0, 1};
			Matrix matrixMirrorY = new Matrix();
			matrixMirrorY.setValues(mirrorY);

			matrix.postConcat(matrixMirrorY);
		}

		// recreate the new Bitmap	
		return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
	}

	/**
	 * Method to create the bitmap on safe way 
	 * @param path the path of the file
	 * @param width the width of the image
	 * @param key the key of the bitmap on lruCache map
	 * @throws NotEnoughMemoryException if the application don't have memory enough, OutOfMemoryError get if the file exceeded the memory
	 */
	public void createBitmapFile(final String path, final int width, final String key) throws NotEnoughMemoryException, OutOfMemoryError {
		if(getBitmapFromMemoryCache(key) == null){

			Bitmap bitmap = decodeSampledBitmapFromFile(path, width);

			if(bitmap != null){

				int height = 0;

				if (width > 0) {
					height = calcHeightImage(bitmap, width);

					if(height > 0){
						bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
					}
				}

				try {
					final Integer oriantationRotate = needRotate(path, bitmap);
					if(oriantationRotate != null){
						bitmap = rotateBitmap(path, bitmap, oriantationRotate);
					} 
				} catch (IOException e) {
				}


				addBitmapToMemoryCache(key, bitmap);
			}
		} 
	}


	private static class NotEnoughMemoryException extends Exception {
		/**
		 * error in FindBugs
		 */
		private static final long serialVersionUID = 1L;

		public NotEnoughMemoryException(final String message) {
			super(message);
		}
	}

	/**
	 * Method to calculate about the new Width and Height
	 * @param options of the file
	 * @param reqWidth the Width required
	 * @param reqHeight the Height required
	 * @return int the normal to calculate the new Width and Height
	 */
	public static int calculateInSampleSize(final BitmapFactory.Options options, final int reqWidth, final int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	/**
	 * Method to create the bitmap on safe way 
	 * @param path the path of the file
	 * @param reqWidth the Width required
	 * @return Bitmap of image created
	 * @throws NotEnoughMemoryException if the application don't have memory enough, OutOfMemoryError get if the file exceeded the memory
	 */
	private static Bitmap decodeSampledBitmapFromFile(final String path, final int reqWidth) throws NotEnoughMemoryException, OutOfMemoryError {

		if(verifyCanCreateFile(path, reqWidth)){

			// First decode with inJustDecodeBounds=true to check dimensions
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			remove("decodeSampledBitmap");
			addBitmapToMemoryCache("decodeSampledBitmap", BitmapFactory.decodeFile(path, options));

			if(reqWidth > 0){
				// Calculate inSampleSize
				final int reqHeight = calcHeight(options.outHeight, options.outWidth, reqWidth);
				options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
			}

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			return BitmapFactory.decodeFile(path, options);
		} else {
			throw new NotEnoughMemoryException("No memory");
		}
	}

	/**
	 * Method to get the InnerLruCache
	 * @return InnerLruCache the instance of lruCache
	 */
	public static InnerLruCache getLruCache(){
		return lruCache;
	}

	/**
	 * Method to get the space used of LruCache
	 * @return int the size used
	 */
	public static int getSize(){
		return lruCache.size();
	}

	/**
	 * Method to get the max size of the LruCache
	 * @return int the total size
	 */
	public static int getMaxSize(){
		return lruCache.maxSize();
	}

	public static Drawable convertBitmapToDrawable(final Context context,final Bitmap bitmap) {
		BitmapDrawable image = null;
		if (bitmap==null) {
			image = (BitmapDrawable) context.getResources().getDrawable(R.drawable.git_icon);
		}else{
			image = new BitmapDrawable(bitmap);
		}
		return image;
	}
}