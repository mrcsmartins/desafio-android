package com.concretesolutions.desafioandroid.model.task;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by Soua on 19/11/2015.
 */
public class RequestTask extends AsyncTask<String, String, String> {

    public interface  IRequestTask {
        void fail(String error);
        void success(String result);
    }

    IRequestTask requestTask;
    public  RequestTask(IRequestTask requestTask){
        this.requestTask = requestTask;
    }


    @Override
    protected String doInBackground(String... uri) {
        HttpClient httpclient = new DefaultHttpClient();
        org.apache.http.HttpResponse response=null;
        String responseString = null;
        try {
            response = httpclient.execute(new HttpGet(uri[0]));

            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                out.close();
                responseString = out.toString();
            } else{
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            //TODO Handle problems..
            if(this.requestTask!=null){
                this.requestTask.fail(e.getMessage());
            }
        } catch (IOException e) {
            //TODO Handle problems..
            if(this.requestTask!=null){
                this.requestTask.fail(e.getMessage());
            }
        }
        return responseString;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(this.requestTask!=null){
            this.requestTask.success(result);
        }
        Log.i("challenge", "result:" + result);
    }
}
