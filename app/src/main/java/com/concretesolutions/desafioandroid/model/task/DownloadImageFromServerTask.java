package com.concretesolutions.desafioandroid.model.task;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;


/**
 * Created by Mauricio on 16/11/2015.
 */

public class DownloadImageFromServerTask extends AsyncTask<String, Void, Bitmap> {

    private IDownloadImageFromServerTask listener;

    public interface IDownloadImageFromServerTask {
        void showDialog();

        void dismissDialog();

        void setImage(Bitmap bitmap);

        void setStandardImage();
    }

    public DownloadImageFromServerTask(IDownloadImageFromServerTask presenter) {
        this.listener = presenter;
    }

    @Override
    protected void onPreExecute() {
        this.listener.showDialog();
        super.onPreExecute();
    }

    protected Bitmap doInBackground(String... urls) {
        String url = urls[0];
        Bitmap bitmap = null;
        try {
            bitmap = LruCacheBitmap.getBitmapFromMemoryCache(url);
           if (bitmap == null) {
                InputStream in = new java.net.URL(url).openStream();
                bitmap = getRoundedShape(BitmapFactory.decodeStream(in));
                LruCacheBitmap.addBitmapToMemoryCache(url, bitmap);
            }
        } catch (Exception e) {
            Log.i("Challenge", "Error:" + e.getMessage());
        }
        return bitmap;
    }

    protected void onPostExecute(Bitmap bitmap) {
        if(bitmap==null) {
            this.listener.setStandardImage();
        }else{
            this.listener.setImage(bitmap);
        }
        this.listener.dismissDialog();
    }

    public static Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
        int targetWidth = 350;
        int targetHeight = 350;

        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2, ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth), ((float) targetHeight)) / 2), Path.Direction.CCW);
        canvas.clipPath(path);

        if (scaleBitmapImage != null && !scaleBitmapImage.isRecycled()) {
            Bitmap sourceBitmap = scaleBitmapImage;
            canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
                    sourceBitmap.getHeight()), new Rect(0, 0, targetWidth, targetHeight), null);
            scaleBitmapImage.recycle();
        }

        return targetBitmap;
    }
}
