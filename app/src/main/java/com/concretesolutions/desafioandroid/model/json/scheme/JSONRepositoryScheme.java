package com.concretesolutions.desafioandroid.model.json.scheme;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mauricio on 16/r11/15.
 */
public class JSONRepositoryScheme {
    @SerializedName("items")
    private List<JSONRepositoryScheme> repositories;


    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("stargazers_count")
    private Integer stargazersCount;

    @SerializedName("forks_count")
    private Integer forksCount;

    @SerializedName("owner")
    JSONOwnerScheme owner;

    public JSONOwnerScheme getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Integer getStargazersCount() {
        return stargazersCount;
    }

    public Integer getForksCount() {
        return forksCount;
    }

    @SerializedName("total_count")
    private Integer totalCount;

    @SerializedName("incompleteResults")
    private Boolean incompleteResults;


    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(Boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public List<JSONRepositoryScheme> getRepositories() {
        return repositories;
    }

    public class JSONOwnerScheme {

        @SerializedName("login")
        private String login;

        @SerializedName("avatar_url")
        private String avatarUrl;

        public String getLogin() {
            return login;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }
    }
}
