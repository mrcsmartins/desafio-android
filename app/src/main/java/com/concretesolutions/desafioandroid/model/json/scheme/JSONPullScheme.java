package com.concretesolutions.desafioandroid.model.json.scheme;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mauricio on 16/11/15.
 */
public class JSONPullScheme {

    @SerializedName("title")
    private String title;

    @SerializedName("created_at")
    private String date;

    @SerializedName("html_url")
    private String url;

    @SerializedName("body")
    private String description;

    public String getUrl() {
        return url;
    }

    @SerializedName("user")
    JSONOwnerScheme user;

    public JSONOwnerScheme getUser() {
        return user;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public class JSONOwnerScheme {

        @SerializedName("login")
        private String login;

        @SerializedName("avatar_url")
        private String avatarUrl;

        public String getLogin() {
            return login;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }
    }
}
